# appium-test-gb
This framework for test "WebDriver I/O" mobile app v0.2.1. 
Tests are run parallel with TestNG in two device.

**Test environment:**    
device_1 - emulator GooglePixel2 API28, Android 9, x86, Build.number=RSR1.180720.117   
device_2 - emulator GooglePixel3 API30, Android 11, x86, Build.number=RSR1.201013.001


**Check-list:**
1. main activity:

    1.1. check UI with screenshot
2. login activity:

    2.1. positive:

    2.1.1. check the form when the fields are filling with valid data

    2.1.2. check UI with screenshot

    2.2. negative

    2.2.1. check the form when the fields are filling with not valid data

3. forms activity

    3.1. positive:

    3.1.1. check Switch Element

    3.1.2. check visible Dropdown-list

    3.1.3. check the selection Dropdown-list item

4. swipe activity

    4.1. positive:

    4.1.1. check the title text of the 3-d card in the carousel

5. tab bar

    5.1. positive:

    5.1.1. check thr color change of icons when tap

command run: mvn clean test        
command report: allure serve target/allure-results

for comments and bug: telegram @W_Ilya
