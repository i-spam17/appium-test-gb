package base;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import helpers.Helpers;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import pages.MainActivity;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseTest {
    public AppiumDriver driver = null;

    public MainActivity openApp(String device) {
        driver = (AppiumDriver) getAndroidDriver(device);
        WebDriverRunner.setWebDriver(driver);
        return new MainActivity();
    }

    private WebDriver getAndroidDriver(String device) {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");

        switch (device){
            case "pixel_2":
                capabilities.setCapability("udid", "emulator-5554");
                break;
            case "pixel_3":
                capabilities.setCapability("udid", "emulator-5556");
                break;
        }

        capabilities.setCapability("app", FileUtils.getFile("src/test/resources/Android-NativeDemoApp-0.2.1.apk").getAbsolutePath());

        Configuration.reportsFolder = "target/screenshots/actual";

        AndroidDriver driver = null;
        try {
            driver = new AndroidDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return driver;
    }

    @AfterMethod
    public void setDown() {
        driver.quit();
    }

    @AfterSuite
    public void setEnvironmentInfo() {
        Helpers.saveEnvironmentFile();
    }
}
