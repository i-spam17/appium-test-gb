package locators;

import com.codeborne.selenide.SelenideElement;
import io.appium.java_client.AppiumBy;

import static com.codeborne.selenide.Selenide.$;

public class LoginPageLocators {
    public SelenideElement loginButton() {
        return $(AppiumBy.accessibilityId("button-LOGIN"));
    }

    public SelenideElement emailErrorText() {
        return $(AppiumBy.xpath("//android.widget.TextView[text(), contains(@text, 'email')]"));
    }

    public SelenideElement passwordErrorText() {
        return $(AppiumBy.xpath("//android.widget.TextView[text(), contains(@text, '8')]"));
    }

    public SelenideElement inputLogin() {
        return $(AppiumBy.accessibilityId("input-email"));
    }

    public SelenideElement inputPassword() {
        return $(AppiumBy.accessibilityId("input-password"));
    }

    public SelenideElement loginActivityScreen() {
        return $(AppiumBy.accessibilityId("Login-screen"));
    }
}
