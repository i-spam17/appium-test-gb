package locators;

import com.codeborne.selenide.SelenideElement;
import io.appium.java_client.AppiumBy;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SwipeActivityLocators {
    public SelenideElement swipeActivityScreen() {
        return $(AppiumBy.accessibilityId("Swipe-screen"));
    }

    public SelenideElement cardTitleText() {
        List<SelenideElement> elements = $$(AppiumBy.xpath("*//*[@content-desc=\"card\"]/*"));
        return elements.get(1);
    }

    public SelenideElement paginationDots(int numberPosition) {
        List<SelenideElement> elements = $$(AppiumBy.xpath("*//*[@content-desc=\"Carousel\"]/android.view.ViewGroup"));
        return elements.get(numberPosition);
    }
}
