package locators;

import com.codeborne.selenide.SelenideElement;
import io.appium.java_client.AppiumBy;

import static com.codeborne.selenide.Selenide.$;

public class TabBarLocators {
    public SelenideElement loginIcon() {
        return $(AppiumBy.accessibilityId("Login"));
    }

    public SelenideElement homeIcon() {
        return $(AppiumBy.accessibilityId("Home"));
    }

    public SelenideElement webViewIcon() {
        return $(AppiumBy.accessibilityId("WebView"));
    }

    public SelenideElement formsIcon() {
        return $(AppiumBy.accessibilityId("Forms"));
    }

    public SelenideElement swipeIcon() {
        return $(AppiumBy.accessibilityId("Swipe"));
    }
}
