package helpers;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ImageComparisonResult;
import com.github.romankh3.image.comparison.model.ImageComparisonState;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.Sequence;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Collections;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class Helpers {
    private static final String expectedPath = "src/main/resources/screenshots/expected/";

    public static void compareScr(SelenideElement element, String nameExpectedImg) {
        String actualPath = "target/screenshots/actual/" + UUID.randomUUID() + ".png";
        String comparePath = "target/screenshots/diff/" + nameExpectedImg + "_diff.png";

        try {
            FileUtils.copyFile(element.getScreenshotAs(OutputType.FILE), new File(actualPath));
            allureSaveScreenshot(Files.readAllBytes(Paths.get(expectedPath + nameExpectedImg)));
            allureSaveScreenshot(Files.readAllBytes(Paths.get(actualPath))); //по сути можно не делать..
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedImage actualImg = ImageComparisonUtil.readImageFromResources(actualPath);
        BufferedImage expectedImg = ImageComparisonUtil.readImageFromResources(expectedPath + nameExpectedImg);
        ImageComparisonResult compareImages = new ImageComparison(expectedImg, actualImg).compareImages();
        if (ImageComparisonState.MATCH != compareImages.getImageComparisonState()) {
            ImageComparisonUtil.saveImage(new File(comparePath), compareImages.getResult());
            try {
                allureSaveScreenshot(Files.readAllBytes(Paths.get(comparePath)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        assertEquals(ImageComparisonState.MATCH, compareImages.getImageComparisonState());
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public static byte[] allureSaveScreenshot(byte[] scr) {
        return scr;
    }

    public static void horizontalSwipe(AppiumDriver driver, SelenideElement element, Direction direction) {
        System.out.println("swipeScreen(): dir: '" + direction + "'"); // always log your actions

        Dimension dims = driver.manage().window().getSize();
        element.shouldBe(Condition.visible);
        int edgeBorder = 10;
        int heightSwipe = dims.height / 2;
        int rightPoint;
        int leftPoint;
        switch (direction) {
            case TO_LEFT: //coordinates right point border
                rightPoint = dims.width - edgeBorder;
                leftPoint = edgeBorder;
                break;
            case TO_RIGHT: // coordinates left point border
                rightPoint = edgeBorder;
                leftPoint = dims.width - edgeBorder;
                break;
            default:
                throw new IllegalArgumentException("swipeScreen(): dir: '" + direction + "' NOT supported");
        }

        PointerInput finger = new PointerInput(PointerInput.Kind.TOUCH, "finger");

        Sequence dragNDrop = new Sequence(finger, 1);
        dragNDrop.addAction(finger.createPointerMove(Duration.ofMillis(0), PointerInput.Origin.viewport(), rightPoint, heightSwipe));
        dragNDrop.addAction(finger.createPointerDown(PointerInput.MouseButton.LEFT.asArg()));
        dragNDrop.addAction(finger.createPointerMove(Duration.ofMillis(700), PointerInput.Origin.viewport(), leftPoint, heightSwipe));
        dragNDrop.addAction(finger.createPointerUp(PointerInput.MouseButton.LEFT.asArg()));
        driver.perform(Collections.singletonList(dragNDrop));

        element.shouldBe(Condition.visible);
    }

    public enum Direction {
        TO_LEFT,
        TO_RIGHT
    }

    public static void swipeSeveralTimes(AppiumDriver driver, SelenideElement element, Direction direction, int n) {
        for (int i = 0; i < n; i++) {
            horizontalSwipe(driver, element, direction);
        }
    }

    public static void saveEnvironmentFile(){
        try {
            FileUtils.copyFile(new File("src/test/resources/environment.properties"), new File("target/allure-results/environment.properties"));
        } catch (IOException e) {
            System.out.println("file \"environment.properties\" not exist");
            e.printStackTrace();
        }
    }
}
