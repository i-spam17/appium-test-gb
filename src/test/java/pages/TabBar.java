package pages;

import helpers.Helpers;
import io.qameta.allure.Step;
import locators.TabBarLocators;

public class TabBar {
    private TabBarLocators locator() {
        return new TabBarLocators();
    }

    @Step("Тап по кнопке \"Login\" в меню и переходим на страницу Login")
    public LoginActivity tapLogin() {
        locator().loginIcon().click();
        return new LoginActivity();
    }

    @Step("Тап по кнопке \"Home\" в меню и переходим на страницу Home")
    public MainActivity tapHome() {
        locator().homeIcon().click();
        return new MainActivity();
    }

    @Step("Сравниваем скриншот иконки Home")
    public void checkIconHomeScreenshot() {
        Helpers.compareScr(locator().homeIcon(), "icon_home.png");
    }

    @Step("Тап по кнопке \"WebView\" в меню и переходим на страницу WebView")
    public WebViewActivity tapWebView() {
        locator().webViewIcon().click();
        return new WebViewActivity();
    }

    @Step("Сравниваем скриншот иконки WebView")
    public void checkIconWebViewScreenshot() {
        Helpers.compareScr(locator().webViewIcon(), "icon_webView.png");
    }

    @Step("Тап по кнопке \"Forms\" в меню и переходим на страницу Forms")
    public FormsActivity tapForms() {
        locator().formsIcon().click();
        return new FormsActivity();
    }

    @Step("Сравниваем скриншот иконки Forms")
    public void checkIconFormsScreenshot() {
        Helpers.compareScr(locator().formsIcon(), "icon_forms.png");
    }

    @Step("Тап по кнопке \"Swipe\" в меню и переходим на страницу Swipe")
    public SwipeActivity tapSwipe() {
        locator().swipeIcon().click();
        return new SwipeActivity();
    }

    @Step("Сравниваем скриншот иконки Swipe")
    public void checkIconSwipeScreenshot() {
        Helpers.compareScr(locator().swipeIcon(), "icon_swipe.png");
    }

    @Step("Сравниваем скриншот иконки Swipe")
    public void checkIconLoginScreenshot() {
        Helpers.compareScr(locator().loginIcon(), "icon_login.png");
    }
}
