package pages;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import locators.WebViewActivityLocators;

public class WebViewActivity extends TabBar {
    private WebViewActivityLocators locator() {
        return new WebViewActivityLocators();
    }

    @Step("Проверка загрузки WebViewActivity")
    public WebViewActivity checkWebViewActivityExist() {
        locator().loadingScreen().shouldBe(Condition.visible);
        return this;
    }
}
