package pages;

import helpers.Helpers;
import io.qameta.allure.Step;
import locators.MainPageLocators;

public class MainActivity extends TabBar {
    MainPageLocators locators() {
        return new MainPageLocators();
    }

    @Step("Делаем скриншот страницы Home и сравниваем с требованием.")
    public MainActivity checkMainScreenshot() {
        Helpers.compareScr(locators().homeScreen(), "mainPage.png");
        return this;
    }
}
