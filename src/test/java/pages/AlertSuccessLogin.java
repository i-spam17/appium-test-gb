package pages;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import locators.AlertSuccessLocators;

public class AlertSuccessLogin {
    private AlertSuccessLocators locator() {
        return new AlertSuccessLocators();
    }

    public AlertSuccessLogin tapAlertSuccessButton() {
        locator().alertBtnOK().click();
        return this;
    }

    @Step("Проверяем текст Success Alert поля")
    public AlertSuccessLogin checkAlertSuccessText(String text) {
        locator().alertMsgTitle().shouldBe(Condition.text(text));
        return new AlertSuccessLogin();
    }

    @Step("Проверяем текст Alert поля")
    public AlertSuccessLogin checkAlertText(String text) {
        locator().alertMsg().shouldBe(Condition.text(text));
        return new AlertSuccessLogin();
    }
}
