package pages;

import com.codeborne.selenide.Condition;
import helpers.Helpers;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import locators.SwipeActivityLocators;

public class SwipeActivity extends TabBar {
    private SwipeActivityLocators locator() {
        return new SwipeActivityLocators();
    }

    @Step("Swipe Activity загрузился")
    public SwipeActivity checkSwipeActivityExist() {
        locator().swipeActivityScreen().shouldBe(Condition.exist);
        return this;
    }

    @Step("Свайп влево")
    public SwipeActivity swipeToTheLeft(AppiumDriver driver) {
        Helpers.horizontalSwipe(driver, locator().swipeActivityScreen(), Helpers.Direction.TO_LEFT);
        return this;
    }

    @Step("Свайп вправо")
    public SwipeActivity swipeToTheRight(AppiumDriver driver) {
        Helpers.horizontalSwipe(driver, locator().swipeActivityScreen(), Helpers.Direction.TO_RIGHT);
        return this;
    }

    @Step("Свайпы влево в карусели")
    public SwipeActivity numberSwipeToTheLeft(AppiumDriver driver, int count) {
        Helpers.swipeSeveralTimes(driver, locator().swipeActivityScreen(), Helpers.Direction.TO_LEFT, count);
        return this;
    }

    @Step("Тап на 3-ю точку под каруселью")
    public SwipeActivity tapThirdDotUnderCarousel() {
        locator().paginationDots(3).click();
        return this;
    }

    @Step("Проверка текста 3-й карточки в карусели")
    public void checkTextTitleCard() {
        locator().cardTitleText().shouldHave(Condition.text("JS.FOUNDATION"));
    }
}
