package tests.forms;

import base.BaseTest;
import jdk.jfr.Description;
import org.testng.annotations.Test;
import pages.FormsDropdownList;

public class PositiveTest extends BaseTest {
    @Test
    @Description("Проверяем переключение SwitchElement")
    public void checkSwitchElement() {
        openApp("pixel_2")
                .tapForms()
                .tapSwitch()
                .checkSwitchElementEnabled();
    }

    @Test
    @Description("Проверяем появление Dropdown списка")
    public void checkDropdownElement() {
        openApp("pixel_3")
                .tapForms()
                .tapDropdownElement();
        new FormsDropdownList().checkDropdownListExist();
    }

    @Test
    @Description("Проверяем отображение выбранного элемента из Dropdown списка")
    public void checkDropdownElementSelected() {
        openApp("pixel_2")
                .tapForms()
                .tapDropdownElement();
        new FormsDropdownList()
                .tapFirstElementFromList()
                .checkSelectedElementFromDropdownList();
    }
}
