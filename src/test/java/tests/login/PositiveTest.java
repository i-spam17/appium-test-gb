package tests.login;

import base.BaseTest;
import io.qameta.allure.Description;
import org.testng.annotations.Test;
import pages.AlertSuccessLogin;

public class PositiveTest extends BaseTest {
    private static final String LOGIN = "qwe@qwe.qwe";
    private static final String PASSWORD = "12345678";
    private static final String ALERT_TITLE = "Success";
    public static final String ALERT_TEXT = "You are logged in!";

    @Test
    @Description("Проверяем работу формы при заполнении полей валидными данными")
    public void positiveLoginTest() {
        openApp("pixel_2")
                .tapLogin()
                .fillInputLogin(LOGIN)
                .fillInputPassword(PASSWORD)
                .tapLoginButton();
        new AlertSuccessLogin().checkAlertSuccessText(ALERT_TITLE).checkAlertText(ALERT_TEXT);
    }

    @Test
    @Description("Проверяем UI страницы Login с помощью скриншота")
    public void CheckLoginPageScreenshot() {
        openApp("pixel_3")
                .tapLogin()
                .checkLoginScreenshot();
    }
}
