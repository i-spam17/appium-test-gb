package tests.main;

import base.BaseTest;
import io.qameta.allure.Description;
import org.testng.annotations.Test;

public class PositiveTest extends BaseTest {
    @Test
    @Description("Проверяем UI главной страницы с помощью скриншота.")
    public void checkMainPageScreenshot() {
        openApp("pixel_3")
                .checkMainScreenshot();
    }
}
